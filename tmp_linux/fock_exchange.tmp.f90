












!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with CPLX and then again without CPLX.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without CPLX.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Calculate the bare exchange (Fock exchange) part of self-energy for all the
! selected diagonal and off-diagonal matrix elements. The bare
! exchange is given in Eq. 23 of Tiago & Chelikowsky, PRB (2006).
!
! OUTPUT:
!    sig%xdiag : diagonal matrix elements of exchange
!    sig%xoffd : off-diagonal matrix elements of exchange
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine dfock_exchange(gvec,kpt,sig,isp,ik)

  use typedefs
  use mpi_module
  use fft_module
  implicit none

  ! arguments
  ! real-space grid
  type (gspace), intent(inout) :: gvec
  ! k-points and electron wavefunctions
  type (kptinfo), intent(in) :: kpt
  ! self-energy for this k-point and this spin channel
  type (siginfo), intent(inout) :: sig
  ! spin channel and k-points
  integer, intent(in) :: isp, ik

  ! local variables
  ! tolerance in the identification of k-points
  real(dp), parameter :: tol_l = 1.d-8
  integer :: ii, jj, nn, icol, m1, m2, info, isig, irp, jk, maxv, ngrid, &
       ngrid_pe, ioff, ipe, icol_pe, s_nmap
  real(dp) :: fac, occfac, qkt(3), qkt_test(3)
  real(dp) :: kernel
  logical, allocatable :: select_map(:)
  integer, allocatable :: s_map(:), mapsigtos(:)
  real(dp), allocatable :: wfn2(:), k_x(:,:), rho_h(:)
  ! external functions
  real(dp), external :: ddot

  !-------------------------------------------------------------------
  ! Initialize variables.
  !
  ngrid_pe = w_grp%ldn * w_grp%npes
  ngrid = w_grp%mydim
  fac = real(gvec%syms%ntrans,dp)/gvec%hcub

  allocate(wfn2(ngrid_pe),stat=info)
  call alccheck('wfn2','fock_exchange',ngrid_pe,info)
  wfn2 = zero

  allocate(select_map(sig%nmap))
  select_map = .false.
  do isig = 1, sig%ndiag_s
     select_map(sig%diag(isig)) = .true.
  enddo
  do isig = 1, sig%noffd_s
     select_map(sig%off1(isig)) = .true.
     select_map(sig%off2(isig)) = .true.
  enddo
  s_nmap = 0
  do ii = 1, sig%nmap
     if (select_map(ii)) s_nmap = s_nmap + 1
  enddo
  allocate(s_map(s_nmap))
  jj = 0
  do ii = 1, sig%nmap
     if (select_map(ii)) then
        jj = jj + 1
        s_map(jj) = sig%map(ii)
     endif
  enddo
  deallocate(select_map)

  allocate(k_x(s_nmap,s_nmap),stat=info)
  call alccheck('k_x','fock_exchange',s_nmap*s_nmap,info)
  k_x = zero

  allocate(rho_h(w_grp%nr),stat=info)
  call alccheck('rho_h','fock_exchange',w_grp%nr,info)

  !-------------------------------------------------------------------
  ! Initialize Coulomb box,
  ! V_coul = 4*pi*e^2/q^2 (rydberg units, e^2 = 2).
  !
  qkt = zero
  call dinitialize_FFT(peinf%inode,fft_box)
  if (gvec%per == 1) then
     call dcreate_coul_1D(peinf%inode,peinf%npes,peinf%comm,qkt(1),gvec,fft_box)
  elseif (gvec%per == 2) then
     call dcreate_coul_2D(gvec%bdot,qkt(1),fft_box)
  else
     call dcreate_coul_0D(gvec%bdot,qkt,fft_box)
  endif

  !-------------------------------------------------------------------
  ! Calculate kernel matrix elements, kernel(n,i,j) = < n m1 | V_coul | n m2 >.
  ! Distribute kernel round-robin over processors. Also, evaluate only
  ! the upper triangle of matrix k_x: k_x(ii,jj) with ii < jj.
  !
  do jk = 1, kpt%nk
     ! Determine the number of occupied orbitals (maxv).
     maxv = 0
     do nn = 1, kpt%wfn(isp,jk)%nstate
        occfac = kpt%wfn(isp,jk)%occ1(nn)
        if (occfac > tol_occ .and. maxv < nn) maxv = nn
     enddo

     do icol_pe = 1, s_nmap * maxv, peinf%npes
        do ipe = 0, w_grp%npes - 1
           icol = icol_pe + ipe + &
                w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
           if (icol > s_nmap * maxv) cycle
           nn = mod(icol-1, maxv) + 1
           jj = (icol-1)/maxv + 1
           ioff = w_grp%ldn * ipe + 1
           m2 = s_map(jj)
           call dcopy(ngrid,kpt%wfn(isp,jk)%dwf(1,nn),1,wfn2(ioff),1)
           wfn2 = (wfn2)
           call dmultiply_vec(ngrid,kpt%wfn(isp,ik)%dwf(1,m2),wfn2(ioff))
        enddo
        call dgather(1,wfn2,rho_h)
        icol = icol_pe + w_grp%inode + &
             w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
        if (icol <= sig%nmap * maxv) then
           nn = mod(icol-1, maxv) + 1
           jj = (icol-1)/maxv + 1
           m2 = s_map(jj)
           irp = gvec%syms%prod(kpt%wfn(isp,jk)%irep(nn),kpt%wfn(isp,ik)%irep(m2))
           ! If necessary, update the Coulomb interaction.
           qkt_test = kpt%fk(:,ik) - kpt%fk(:,jk) - qkt

           if (dot_product(qkt_test,qkt_test) > tol_l) then
              if (peinf%master) then
                 write(6,*) ' WARNING: q-point has changed '
                 write(6,*) ik, kpt%fk(:,ik)
                 write(6,*) jk, kpt%fk(:,jk)
                 write(6,*) ' old q-vector ', qkt
                 write(6,*) ' new q-vector ', kpt%fk(:,ik) - kpt%fk(:,jk)
              endif
              qkt = kpt%fk(:,ik) - kpt%fk(:,jk)
              if (gvec%per == 1) then
                 call dcreate_coul_1D(peinf%inode,peinf%npes,peinf%comm,qkt(1),gvec,fft_box)
              elseif (gvec%per == 2) then
                 call dcreate_coul_2D(gvec%bdot,qkt(1),fft_box)
              else
                 call dcreate_coul_0D(gvec%bdot,qkt,fft_box)
              endif
           endif

           call dpoisson(gvec,rho_h,irp)
        endif
        call dscatter(1,wfn2,rho_h)
        do ipe = 0, w_grp%npes - 1
           icol = icol_pe + ipe + &
                w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
           if (icol > s_nmap * maxv) cycle
           nn = mod(icol-1, maxv) + 1
           jj = (icol-1)/maxv + 1
           m2 = s_map(jj)
           ioff = w_grp%ldn * ipe + 1
           call dmultiply_vec(ngrid,kpt%wfn(isp,jk)%dwf(1,nn),wfn2(ioff))
           do ii = 1, jj
              m1 = s_map(ii)
              irp = gvec%syms%prod(kpt%wfn(isp,ik)%irep(m1),kpt%wfn(isp,ik)%irep(m2))
              if (irp /= 1) cycle
              kernel = -ddot(ngrid,kpt%wfn(isp,ik)%dwf(1,m1),1,wfn2(ioff),1) * fac
              occfac = kpt%wfn(isp,jk)%occ1(nn) / real(kpt%nk,dp)
              k_x(ii,jj) = k_x(ii,jj) + kernel * occfac
              if (ii == jj) write(6,*) ' K_X ', jk, m1, nn, kernel*ryd
           enddo
        enddo
     enddo
  enddo

  call dfinalize_FFT(peinf%inode,fft_box)

  do jj = 1, s_nmap
     do ii = 1, jj - 1
        k_x(jj,ii) = (k_x(ii,jj))
     enddo
  enddo

  allocate(mapsigtos(sig%nmap))
  call mapinverse(1,sig%nmap,sig%map,s_nmap,s_map,mapsigtos)
  do isig = 1, sig%ndiag_s
     ii = mapsigtos(sig%diag(isig))
     sig%xdiag(isig) = k_x(ii,ii)
  enddo

  do isig = 1, sig%noffd_s
     ii = mapsigtos(sig%off1(isig))
     jj = mapsigtos(sig%off2(isig))
     sig%xoffd(isig) = k_x(ii,jj)
  enddo
  deallocate(mapsigtos)
  deallocate(rho_h)
  deallocate(k_x)
  deallocate(s_map)
  deallocate(wfn2)

  !-------------------------------------------------------------------
  ! Sum data across PE.
  !
  if (sig%ndiag_s > 0) call zpsum(sig%ndiag_s,peinf%npes,peinf%comm,sig%xdiag)
  if (sig%noffd_s > 0) call zpsum(sig%noffd_s,peinf%npes,peinf%comm,sig%xoffd)

  !-------------------------------------------------------------------
  ! If periodic system, include by hand the long wavelength contribution
  ! of Coulomb kernel. It affects only the diagonal part of occupied levels.
  !
  if (gvec%per > 0) then
     do isig = 1, sig%ndiag_s
        sig%xdiag(isig) = sig%xdiag(isig) - gvec%long * &
             kpt%wfn(isp,ik)%occ1(sig%map(sig%diag(isig)))
     enddo
  endif

end subroutine dfock_exchange
!===================================================================













!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with 1 and then again without 1.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without 1.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Calculate the bare exchange (Fock exchange) part of self-energy for all the
! selected diagonal and off-diagonal matrix elements. The bare
! exchange is given in Eq. 23 of Tiago & Chelikowsky, PRB (2006).
!
! OUTPUT:
!    sig%xdiag : diagonal matrix elements of exchange
!    sig%xoffd : off-diagonal matrix elements of exchange
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine zfock_exchange(gvec,kpt,sig,isp,ik)

  use typedefs
  use mpi_module
  use fft_module
  implicit none

  ! arguments
  ! real-space grid
  type (gspace), intent(inout) :: gvec
  ! k-points and electron wavefunctions
  type (kptinfo), intent(in) :: kpt
  ! self-energy for this k-point and this spin channel
  type (siginfo), intent(inout) :: sig
  ! spin channel and k-points
  integer, intent(in) :: isp, ik

  ! local variables
  ! tolerance in the identification of k-points
  real(dp), parameter :: tol_l = 1.d-8
  integer :: ii, jj, nn, icol, m1, m2, info, isig, irp, jk, maxv, ngrid, &
       ngrid_pe, ioff, ipe, icol_pe, s_nmap
  real(dp) :: fac, occfac, qkt(3), qkt_test(3)
  complex(dpc) :: kernel
  logical, allocatable :: select_map(:)
  integer, allocatable :: s_map(:), mapsigtos(:)
  complex(dpc), allocatable :: wfn2(:), k_x(:,:), rho_h(:)
  ! external functions
  complex(dpc), external :: zdot_c

  !-------------------------------------------------------------------
  ! Initialize variables.
  !
  ngrid_pe = w_grp%ldn * w_grp%npes
  ngrid = w_grp%mydim
  fac = real(gvec%syms%ntrans,dp)/gvec%hcub

  allocate(wfn2(ngrid_pe),stat=info)
  call alccheck('wfn2','fock_exchange',ngrid_pe,info)
  wfn2 = zzero

  allocate(select_map(sig%nmap))
  select_map = .false.
  do isig = 1, sig%ndiag_s
     select_map(sig%diag(isig)) = .true.
  enddo
  do isig = 1, sig%noffd_s
     select_map(sig%off1(isig)) = .true.
     select_map(sig%off2(isig)) = .true.
  enddo
  s_nmap = 0
  do ii = 1, sig%nmap
     if (select_map(ii)) s_nmap = s_nmap + 1
  enddo
  allocate(s_map(s_nmap))
  jj = 0
  do ii = 1, sig%nmap
     if (select_map(ii)) then
        jj = jj + 1
        s_map(jj) = sig%map(ii)
     endif
  enddo
  deallocate(select_map)

  allocate(k_x(s_nmap,s_nmap),stat=info)
  call alccheck('k_x','fock_exchange',s_nmap*s_nmap,info)
  k_x = zzero

  allocate(rho_h(w_grp%nr),stat=info)
  call alccheck('rho_h','fock_exchange',w_grp%nr,info)

  !-------------------------------------------------------------------
  ! Initialize Coulomb box,
  ! V_coul = 4*pi*e^2/q^2 (rydberg units, e^2 = 2).
  !
  qkt = zero
  call zinitialize_FFT(peinf%inode,fft_box)
  if (gvec%per == 1) then
     call zcreate_coul_1D(peinf%inode,peinf%npes,peinf%comm,qkt(1),gvec,fft_box)
  elseif (gvec%per == 2) then
     call zcreate_coul_2D(gvec%bdot,qkt(1),fft_box)
  else
     call zcreate_coul_0D(gvec%bdot,qkt,fft_box)
  endif

  !-------------------------------------------------------------------
  ! Calculate kernel matrix elements, kernel(n,i,j) = < n m1 | V_coul | n m2 >.
  ! Distribute kernel round-robin over processors. Also, evaluate only
  ! the upper triangle of matrix k_x: k_x(ii,jj) with ii < jj.
  !
  do jk = 1, kpt%nk
     ! Determine the number of occupied orbitals (maxv).
     maxv = 0
     do nn = 1, kpt%wfn(isp,jk)%nstate
        occfac = kpt%wfn(isp,jk)%occ1(nn)
        if (occfac > tol_occ .and. maxv < nn) maxv = nn
     enddo

     do icol_pe = 1, s_nmap * maxv, peinf%npes
        do ipe = 0, w_grp%npes - 1
           icol = icol_pe + ipe + &
                w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
           if (icol > s_nmap * maxv) cycle
           nn = mod(icol-1, maxv) + 1
           jj = (icol-1)/maxv + 1
           ioff = w_grp%ldn * ipe + 1
           m2 = s_map(jj)
           call zcopy(ngrid,kpt%wfn(isp,jk)%zwf(1,nn),1,wfn2(ioff),1)
           wfn2 = conjg(wfn2)
           call zmultiply_vec(ngrid,kpt%wfn(isp,ik)%zwf(1,m2),wfn2(ioff))
        enddo
        call zgather(1,wfn2,rho_h)
        icol = icol_pe + w_grp%inode + &
             w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
        if (icol <= sig%nmap * maxv) then
           nn = mod(icol-1, maxv) + 1
           jj = (icol-1)/maxv + 1
           m2 = s_map(jj)
           irp = gvec%syms%prod(kpt%wfn(isp,jk)%irep(nn),kpt%wfn(isp,ik)%irep(m2))
           ! If necessary, update the Coulomb interaction.
           qkt_test = kpt%fk(:,ik) - kpt%fk(:,jk) - qkt

           if (dot_product(qkt_test,qkt_test) > tol_l) then
              if (peinf%master) then
                 write(6,*) ' WARNING: q-point has changed '
                 write(6,*) ik, kpt%fk(:,ik)
                 write(6,*) jk, kpt%fk(:,jk)
                 write(6,*) ' old q-vector ', qkt
                 write(6,*) ' new q-vector ', kpt%fk(:,ik) - kpt%fk(:,jk)
              endif
              qkt = kpt%fk(:,ik) - kpt%fk(:,jk)
              if (gvec%per == 1) then
                 call zcreate_coul_1D(peinf%inode,peinf%npes,peinf%comm,qkt(1),gvec,fft_box)
              elseif (gvec%per == 2) then
                 call zcreate_coul_2D(gvec%bdot,qkt(1),fft_box)
              else
                 call zcreate_coul_0D(gvec%bdot,qkt,fft_box)
              endif
           endif

           call zpoisson(gvec,rho_h,irp)
        endif
        call zscatter(1,wfn2,rho_h)
        do ipe = 0, w_grp%npes - 1
           icol = icol_pe + ipe + &
                w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
           if (icol > s_nmap * maxv) cycle
           nn = mod(icol-1, maxv) + 1
           jj = (icol-1)/maxv + 1
           m2 = s_map(jj)
           ioff = w_grp%ldn * ipe + 1
           call zmultiply_vec(ngrid,kpt%wfn(isp,jk)%zwf(1,nn),wfn2(ioff))
           do ii = 1, jj
              m1 = s_map(ii)
              irp = gvec%syms%prod(kpt%wfn(isp,ik)%irep(m1),kpt%wfn(isp,ik)%irep(m2))
              if (irp /= 1) cycle
              kernel = -zdot_c(ngrid,kpt%wfn(isp,ik)%zwf(1,m1),1,wfn2(ioff),1) * fac
              occfac = kpt%wfn(isp,jk)%occ1(nn) / real(kpt%nk,dp)
              k_x(ii,jj) = k_x(ii,jj) + kernel * occfac
              if (ii == jj) write(6,*) ' K_X ', jk, m1, nn, kernel*ryd
           enddo
        enddo
     enddo
  enddo

  call zfinalize_FFT(peinf%inode,fft_box)

  do jj = 1, s_nmap
     do ii = 1, jj - 1
        k_x(jj,ii) = conjg(k_x(ii,jj))
     enddo
  enddo

  allocate(mapsigtos(sig%nmap))
  call mapinverse(1,sig%nmap,sig%map,s_nmap,s_map,mapsigtos)
  do isig = 1, sig%ndiag_s
     ii = mapsigtos(sig%diag(isig))
     sig%xdiag(isig) = k_x(ii,ii)
  enddo

  do isig = 1, sig%noffd_s
     ii = mapsigtos(sig%off1(isig))
     jj = mapsigtos(sig%off2(isig))
     sig%xoffd(isig) = k_x(ii,jj)
  enddo
  deallocate(mapsigtos)
  deallocate(rho_h)
  deallocate(k_x)
  deallocate(s_map)
  deallocate(wfn2)

  !-------------------------------------------------------------------
  ! Sum data across PE.
  !
  if (sig%ndiag_s > 0) call zpsum(sig%ndiag_s,peinf%npes,peinf%comm,sig%xdiag)
  if (sig%noffd_s > 0) call zpsum(sig%noffd_s,peinf%npes,peinf%comm,sig%xoffd)

  !-------------------------------------------------------------------
  ! If periodic system, include by hand the long wavelength contribution
  ! of Coulomb kernel. It affects only the diagonal part of occupied levels.
  !
  if (gvec%per > 0) then
     do isig = 1, sig%ndiag_s
        sig%xdiag(isig) = sig%xdiag(isig) - gvec%long * &
             kpt%wfn(isp,ik)%occ1(sig%map(sig%diag(isig)))
     enddo
  endif

end subroutine zfock_exchange
!===================================================================
